function getArrWithLeastSum(arr) {
   var minSum = getSumOfProducts(arr);
   for (var i = 0; i < arr.length; i++) {
      for (var j = i+1; j < arr.length; j++) {
         var res = checkSumOfProductsForSwitch(arr, i, j, minSum);
         arr = res[0];
         minSum = res[1];
      }
   }
   return [arr, minSum];
}

function checkSumOfProductsForSwitch(arr, i, j, oldSum) {
   var newArr = swap(arr, i, j);
   var newSum = getSumOfProducts(newArr);
   if (newSum < oldSum) {
      return [newArr, newSum];
   } else {
      return [arr, oldSum];
   }
}

function swap(arr, i, j) {
   var clonedArr = JSON.parse(JSON.stringify(arr));
   var temp = clonedArr[i];
   clonedArr[i] = clonedArr[j];
   clonedArr[j] = temp;
   return clonedArr;
}

function getSumOfProducts(arr) {
   var sum = 0;
   for (var i = 0; i < arr.length -1; i++) {
      sum += arr[i] * arr[i+1];
   }
   return sum;
}

module.exports.getArrWithLeastSum = getArrWithLeastSum;
module.exports.checkSumOfProductsForSwitch = checkSumOfProductsForSwitch;
module.exports.swap = swap;
module.exports.getSumOfProducts = getSumOfProducts;