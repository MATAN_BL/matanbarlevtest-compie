var main = require('./theoretical');

var arr = [1,1,20,3,46, 22];
arr = [100,2, 7, 4, 21, 77, 92];

testGivenArray(arr);

function testGivenArray(arr) {
   var bestRes = main.getArrWithLeastSum(arr);
   var bestSum = bestRes[1];
   console.log(bestRes);

   console.log("******* getRandomOrder**************");
   testResult(arr, bestSum);
}

// in the following function we create 100 permutations for arr, and check if there is one
// permutation that gives a better result than the result of main.getArrayWithLeastSum(arr)

function testResult(arr, bestSum) {
   for (var j = 0; j < 100; j ++) {
      var flag = 'success';
      var anotherOrderSum = main.getSumOfProducts(getRandomOrder(arr));
      console.log(anotherOrderSum);
      if (anotherOrderSum < bestSum) {
         flag = 'failure';
      };
   }
   console.warn(flag);
}


function getRandomOrder(arr) {
   var naturalNumbersArr = [];
   var newArr = [];

   for (var i = 0; i < arr.length; i++) {
      naturalNumbersArr.push(i);
   }

   while (true) {
      var index = Math.floor(Math.random() * naturalNumbersArr.length);
      var newInd = naturalNumbersArr[index];
      naturalNumbersArr.splice(index,1);
      newArr.push(arr[newInd]);
      if (naturalNumbersArr.length <= 0) {
         break;
      }
   }
   return newArr;
}