(function() {

   var numOfLinesInit = 20;
   var i = 0;
   var bottomReach = 0;
   var waitForPress = false;

   $('document').ready(function() {
      initGrid();

      function initGrid() {
         for (i = 0; i < numOfLinesInit; i++) {
            createRow(i, false);
         }
      }

      $(window).scroll(function() {
         if(($(window).scrollTop() + $(window).height() == getDocHeight()) && waitForPress == false) {
            gotToBottom();
         }
      });

   });

   function gotToBottom() {
      if (bottomReach < 2) { //
         addThreeRows();
         bottomReach++;
      }
      if (bottomReach >= 2 && bottomReach < 4) {
         addDisplayMoreButton();
         waitForPress = true;
      }
   }

   function addDisplayMoreButton() {
      var buttonElem = $('<button id="displayMore"> Display More ... </button>');
      buttonElem.click(buttonPress);
      $('body').append(buttonElem);
      waitForPress = true;

      function buttonPress() {
         $('#displayMore').remove();
         addThreeRows();
         bottomReach++;
         waitForPress = false;
      }
   }

   function addThreeRows() {
      createRow(i++, true);
      createRow(i++, true);
      createRow(i++, true);
   }

   function getDocHeight() {
      var D = document;
      return Math.max(
         D.body.scrollHeight, D.documentElement.scrollHeight,
         D.body.offsetHeight, D.documentElement.offsetHeight,
         D.body.clientHeight, D.documentElement.clientHeight
      );
   }

   function createRow(i, onload) {
      var getRandMode = Math.floor(Math.random() * 3);
      var rowElem = $('<div class=row></div>');
      switch(getRandMode) {
         case 0: // three small pictures
            rowElem.append(getNextImgJQElem('small', i, onload));
            rowElem.append(getNextImgJQElem('small', i, onload));
            rowElem.append(getNextImgJQElem('small', i, onload));
            break;
         case 1: // one big and one small
            rowElem.append(getNextImgJQElem('big', i, onload));
            rowElem.append(getNextImgJQElem('small', i, onload));
            break;
         case 2: // one small and one big
            rowElem.append(getNextImgJQElem('small', i, onload));
            rowElem.append(getNextImgJQElem('big', i, onload));
            break;
      }
      $('#grid').append(rowElem);
   }

   function getNextImgJQElem(size, i, onload) {

      var imgNum = Math.floor(Math.random() * 3) + 1;
      var picClass = " class = 'pic ";
      ((size == 'big') ? (picClass += "big' ") : (picClass += "small'"));
      var newImg = $("<div " + picClass + "><h3>row" + i + "</h3></div>");
      if (onload == true) {
         debugger;
         newImg.addClass('onload');
      }
      newImg.css('background-image', 'url("img/image' + imgNum + '.jpg")');
      console.log(newImg[0]);
      return newImg;
   }

})();